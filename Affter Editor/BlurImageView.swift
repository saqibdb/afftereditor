//
//  BlurImageView.swift
//  Affter Editor
//
//  Created by Roman on 11.10.2017.
//  Copyright © 2017 Red Owl. All rights reserved.
//

import UIKit

class BlurImageView: UIImageView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    override init(image: UIImage?) {
        super.init(image: image)
    }
    
    override init(image: UIImage?, highlightedImage: UIImage?) {
        super.init(image: image, highlightedImage: highlightedImage)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func draw(_ rect: CGRect) {
        let context = UIGraphicsGetCurrentContext()
        
        context?.beginPath()
        context?.setLineWidth(BlurImage.lineWidth)
        context?.setLineCap(.round)
        
        for curveLine in BlurImage.lines {
        for line in curveLine.getLines() {
            context!.setLineWidth(line.lineWidth)
            context?.move(to: CGPoint(x: line.start.x, y: line.start.y))
            context?.addLine(to: CGPoint(x: line.end.x, y: line.end.y))
            context?.setStrokeColor(line.color.cgColor)
            context?.setBlendMode(.clear)
            context?.strokePath()
            
            UIColor.white.set()
        }
        }
    }
    
    func blur(){
        UIGraphicsBeginImageContext(frame.size)
        
        let  context = UIGraphicsGetCurrentContext()
        
        image?.draw(in: CGRect(x: 0, y: 0, width: self.bounds.width, height: self.bounds.height))
        
        context?.beginPath()
        context?.setLineWidth(BlurImage.lineWidth)
        context?.setLineCap(.round)
        
        for curveLine in BlurImage.lines {
        for line in curveLine.getLines() {
            context!.setLineWidth(line.lineWidth)
            context?.move(to: CGPoint(x: line.start.x, y: line.start.y))
            context?.addLine(to: CGPoint(x: line.end.x, y: line.end.y))
            context?.setStrokeColor(line.color.cgColor)
            
            UIColor.clear.set()
            
            context?.strokePath()
        }
        }
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        
        self.image = newImage
    }
    

}
