
import Foundation
import UIKit

class Line: NSObject {
    var start: CGPoint
    var end: CGPoint
    var color: UIColor
    var lineWidth: CGFloat
    
    init(start: CGPoint, end: CGPoint, color: UIColor!, lineWidth: CGFloat) {
        self.start = start
        self.end = end
        self.color = color
        self.lineWidth = lineWidth
    }
    
    func contains(line: Line) -> Bool {
        
        let R0 = self.lineWidth/2
        let R1 = line.lineWidth/2
        
        let x0 = self.start.x
        let x1 = line.start.x
        
        let y0 = self.start.y
        let y1 = line.start.y
        
        let lowerBound = pow(R0 - R1, 2)
        let upperBound = pow(R0 + R1, 2)
        
        let distance = pow(x0 - x1, 2) + pow(y0 - y1, 2)
        
        return lowerBound <= distance && distance <= upperBound
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        let line = object as! Line
        
        return start == line.start && end == line.end
    }
}
