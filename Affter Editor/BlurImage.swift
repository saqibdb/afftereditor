//
//  BlurImage.swift
//  Affter Editor
//
//  Created by Roman on 11.10.2017.
//  Copyright © 2017 Red Owl. All rights reserved.
//

import UIKit

class BlurImage: UIImage {
    
    static var lines: [CurveLine] = []
    
    static var lineWidth: CGFloat = 10
    
    override func draw(in rect: CGRect) {
        UIGraphicsBeginImageContext(rect.size)
        
        let context = UIGraphicsGetCurrentContext()
        
        context?.beginPath()
        context?.setLineWidth(BlurImage.lineWidth)
        context?.setLineCap(.round)
        
        for curveLine in  BlurImage.lines {
            for line in curveLine.getLines() {
                context!.setLineWidth(line.lineWidth)
                context?.move(to: CGPoint(x: line.start.x, y: line.start.y))
                context?.addLine(to: CGPoint(x: line.end.x, y: line.end.y))
                //context?.setStrokeColor(line.color.cgColor)
                context?.setStrokeColor(UIColor.yellow.cgColor)
                context?.strokePath()
                
                UIColor.black.set()
                
            }
        }
    }
    
}
