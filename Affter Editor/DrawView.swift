//
//  DrawImage.swift
//  Affter Editor
//
//  Created by Ilya Kozlov on 08.10.17.
//  Copyright © 2017 Red Owl. All rights reserved.
//

import UIKit

protocol BlurActivationDelegate{
    func applyBlur()
}

class DrawView: UIView {
    
    private var curveLines: [CurveLine] = [CurveLine()]
    
    private var lines: [Line] = []
    
    private var lastPoint: CGPoint!
    
    var drawCircle: UIImageView = UIImageView()
    
    var haveLines: Bool {
        get {
            return curveLines.count != 1
        }
    }
    
    var blurActivationDelegate: BlurActivationDelegate!
    
    var notBLurringContext: CGContext!
    
    var useBrush: Bool = true{
        didSet{
            if useBrush {
                self.drawColor = .green
                drawCircle.removeFromSuperview()
                drawCircle = UIImageView(frame: CGRect(x: bounds.width/2, y: bounds.height/2 , width: lineWidth, height: lineWidth))
                drawCircle.image = #imageLiteral(resourceName: "brush-1")
                
                self.addSubview(drawCircle)
            }else{
                self.drawColor = .clear
                drawCircle.removeFromSuperview()
                drawCircle = UIImageView(frame: CGRect(x: bounds.width/2, y: bounds.height/2 , width: lineWidth, height: lineWidth))
                drawCircle.image = #imageLiteral(resourceName: "eraser-1")
                
                self.addSubview(drawCircle)
            }
        }
    }
    
    var isDrawing: Bool = true
    
    var drawColor = UIColor.green.withAlphaComponent(1)
    
    var lineWidth: CGFloat = 40{
        didSet{
            BlurImage.lineWidth = lineWidth
            drawCircle.removeFromSuperview()
            drawCircle = UIImageView(frame: CGRect(x: bounds.width/2, y: bounds.height/2 , width: lineWidth, height: lineWidth))
            if useBrush {
                drawCircle.image = #imageLiteral(resourceName: "brush-1")
            } else {
                drawCircle.image = #imageLiteral(resourceName: "eraser-1")
            }
            
            self.addSubview(drawCircle)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let newPoint = (touches.first as AnyObject).location(in: self)
        
        let line = Line(start: lastPoint, end: newPoint, color: drawColor, lineWidth: lineWidth)
        
        lastPoint  = newPoint
        
        if !useBrush{
            drawCircle.center = newPoint
            for curveLine in self.curveLines {
                for index in stride(from: curveLine.getLines().count - 1, to: 0, by: -1){
                    if curveLine.getLines()[index].contains(line: line) {
                        curveLine.remove(at: index)
                        
                        BlurImage.lines = self.curveLines
                        
                        drawCircle.center = newPoint

                        self.setNeedsDisplay()
                    }
                }
            }
        } else {
            
            curveLines.last!.addLine(line: line)
            
            drawCircle.center = newPoint
            
            BlurImage.lines = curveLines
            
            lastPoint = newPoint
            
            self.setNeedsDisplay()
        }
    }
    
    override func draw(_ rect: CGRect) {
        var context = UIGraphicsGetCurrentContext()
        
        context?.beginPath()
        context?.setLineWidth(lineWidth)
        context?.setLineCap(.round)
        
        for curve in curveLines {
            for line in curve.getLines() {
                context!.setLineWidth(line.lineWidth)
                context?.move(to: CGPoint(x: line.start.x, y: line.start.y))
                context?.addLine(to: CGPoint(x: line.end.x, y: line.end.y))
                context?.setStrokeColor(line.color.cgColor)
                context?.strokePath()
                notBLurringContext = context
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        lastPoint = (touches.first as AnyObject).location(in: self)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        //blurActivationDelegate.applyBlur(notBluringContext: notBLurringContext)
        curveLines.append(CurveLine())
        
        blurActivationDelegate.applyBlur()
    }
    
    func undoAll() {
        
        self.curveLines.removeAll()
        
        self.curveLines.append(CurveLine())
        
        BlurImage.lines = curveLines
        
        self.setNeedsDisplay()
    }
    
    func undoLast() {
        self.curveLines.removeLast()
        
        if curveLines.isEmpty {
            self.curveLines.append(CurveLine())
        } else {
            self.curveLines.removeLast()
            self.curveLines.append(CurveLine())
        }
        
        BlurImage.lines = curveLines
        
        self.setNeedsDisplay()
    }
    
    func changeColor(color: UIColor?) {
        for curve in curveLines {
            for line in curve.getLines() {
                line.color = color!
            }
        }
        BlurImage.lines = curveLines
        self.setNeedsDisplay()
    }
    
}
