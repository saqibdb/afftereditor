//
//  EraseImageView.swift
//  Affter Editor
//
//  Created by Roman on 13.10.2017.
//  Copyright © 2017 Red Owl. All rights reserved.
//

import Foundation
import UIKit

class EraseImageView: UIView{
    var context: CGContext!
    var contextBounds: CGRect!
    
    var backgroundImage: UIImage!
    var foregroundImage: UIImage!
    var touchWidth: CGFloat!
    var touchRevealsImage: Bool!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.createBitmapContext()
        touchWidth = 10
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.createBitmapContext()
        self.touchWidth = 10
    }
    
    func resetDrawing () {
        //var color = [(self.touchRevealsImage ? 0.0 : 1.0), 1.0]
    }
    
    private func createBitmapContext () {
        
        let grayscale = CGColorSpaceCreateDeviceGray()
        contextBounds = bounds
        context = CGContext.init(data: nil,
                                 width: Int(contextBounds.width),
                                 height: Int(contextBounds.height),
                                 bitsPerComponent: 8,
                                 bytesPerRow: Int(contextBounds.width),
                                 space: grayscale,
                                 bitmapInfo: CGImageAlphaInfo.none.rawValue)
        context.setFillColor(UIColor.white.cgColor)
        context.fill(contextBounds)
        context.setLineCap(CGLineCap.round)
        context.setLineJoin(CGLineJoin.round)
    }
    
    private func drawImageScaled (_ image: UIImage) {
        let selfRatio: CGFloat = self.frame.size.width/self.frame.size.height
        let imgRatio: CGFloat = image.size.width/image.size.height
        var rect: CGRect = CGRect.zero
        
        if selfRatio > imgRatio{
            rect.size.height = self.frame.size.height
            rect.size.width = imgRatio * rect.size.height
        } else {
            rect.size.width = self.frame.size.width
            rect.size.height = rect.size.width/imgRatio
        }
        
        rect.origin.x = 0.5*(self.frame.size.width - rect.size.width)
        rect.origin.y = 0.5*(self.frame.size.height - rect.size.height)
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch: UITouch = touches.first!
        let points: [CGPoint] = [touch.previousLocation(in: self), touch.location(in: self)]
        context.setLineWidth(touchWidth)
        var color: [CGFloat] = []
        
        
        if touchRevealsImage{
            color.append(1)
        }else{
            color.append(0)
        }
        
        color.append(1)
        
        context.setStrokeColor(color)
        context.strokeLineSegments(between: points )
        setNeedsDisplay()
    }
    
    func setImage(image: UIImage) {
        self.backgroundImage = image
        self.foregroundImage = image
        
        self.setNeedsDisplay()
    }
    
    func setTouchRevealsImage(value: Bool){
        self.touchRevealsImage = value
        
        self.setNeedsDisplay()
    }
    
    override func draw(_ rect: CGRect) {
        self.drawImageScaled(self.backgroundImage)
        
        var mask: CGImage = context.makeImage()!
        
        let ctx = UIGraphicsGetCurrentContext()
        ctx?.saveGState()
        ctx?.clip(to: contextBounds, mask: mask)
        
        self.drawImageScaled(self.foregroundImage)
        
        ctx?.restoreGState()
    }
    
    /*- (void)drawRect:(CGRect)rect
    {
    if (self.foregroundImage==nil || self.backgroundImage==nil) return;
    
    // draw background image
    [self drawImageScaled:self.backgroundImage];
    
    // create an image mask from the context
    CGImageRef mask=CGBitmapContextCreateImage(context);
    
    // set the current clipping mask to the image
    CGContextRef ctx=UIGraphicsGetCurrentContext();
    CGContextSaveGState(ctx);
    
    CGContextClipToMask(ctx, contextBounds, mask);
    
    // now draw image (with mask)
    [self drawImageScaled:self.foregroundImage];
    
    CGContextRestoreGState(ctx);
    
    CGImageRelease(mask);
    
    }*/
}

