//
//  UIImage + Extension.swift
//  Affter Editor
//
//  Created by Macintosh on 05.10.17.
//  Copyright © 2017 Red Owl. All rights reserved.
//

import Foundation
import UIKit

extension UIImage
{
    static func imageWithImage(image: UIImage, scaledToSize newSize: CGSize) -> UIImage
    {
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!;
    }
}

extension UIImageView {
    func erase(from startPoint: CGPoint, to endPoint: CGPoint, context: CGContext, lineWidth: CGFloat) {
        
        image?.draw(at: CGPoint())
        
        context.beginPath()
        context.setLineCap(.round)
        
        
       
        if Double((self.image?.size.height)!) < Double((self.image?.size.width)!) {
            print("Width is greater")
        }
        
        
        var correctStart = CGPoint(x: startPoint.x - (self.bounds.width - image!.size.width)/2, y: startPoint.y - (self.bounds.height - image!.size.height)/2)
        var correctEnd = CGPoint(x: endPoint.x - (self.bounds.width - image!.size.width)/2, y: endPoint.y - (self.bounds.height - image!.size.height)/2)
        
        
        
        
        
        if Double((self.image?.size.height)!) > Double((self.image?.size.width)!) {

           
            
            let xStartPoint1 : CGFloat = (startPoint.x - (self.bounds.width - image!.size.width)/2)
            let xStartPoint2 : CGFloat =  ((self.bounds.width/2) - startPoint.x)
            let xStartPoint : CGFloat =  xStartPoint1 - (xStartPoint2/((self.image?.size.height)! / (self.image?.size.width)!))

            
            let yStartPoint1 : CGFloat = (startPoint.y - (self.bounds.height - image!.size.height)/2)
            let yStartPoint2 : CGFloat =  ((self.bounds.height/2) - startPoint.y)
            let yStartPoint : CGFloat =  yStartPoint1 - (yStartPoint2/((self.image?.size.height)! / (self.image?.size.width)!))
            
            
            
            let xEndPoint1 : CGFloat = (endPoint.x - (self.bounds.width - image!.size.width)/2)
            let xEndPoint2 : CGFloat =  ((self.bounds.width/2) - endPoint.x)
            let xEndPoint : CGFloat =  xEndPoint1 - (xEndPoint2/((self.image?.size.height)! / (self.image?.size.width)!))
            
            
            let yEndPoint1 : CGFloat = (endPoint.y - (self.bounds.height - image!.size.height)/2)
            let yEndPoint2 : CGFloat =  ((self.bounds.height/2) - endPoint.y)
            let yEndPoint : CGFloat =  yEndPoint1 - (yEndPoint2/((self.image?.size.height)! / (self.image?.size.width)!))
            
            

            print("Points are \(xStartPoint), \(yStartPoint) , \(xEndPoint), \(yEndPoint)")
            
            
            
            correctStart = CGPoint(x: xStartPoint, y: yStartPoint)
        
            
            correctEnd = CGPoint(x: xEndPoint, y: yEndPoint)

            
            
            //correctStart = CGPoint(x: (startPoint.x - (self.bounds.width - image!.size.width)/2) - ((self.bounds.width - image!.size.width)/2) - startPoint.x), y: (startPoint.y - (self.bounds.height - image!.size.height)/2) - ())
            
            
            
            
            
            //correctEnd = CGPoint(x: (endPoint.x - (self.bounds.width - image!.size.width)/2) - ((image!.size.width)/10), y: (endPoint.y - (self.bounds.height - image!.size.height)/2) - ((image!.size.height)/10))
        
        
        }
        
        
        
        
        context.move(to: correctStart)
        context.addLine(to: correctEnd)
        context.setLineWidth(lineWidth)
        context.setBlendMode(.clear)
        context.strokePath()
        
        self.image = UIGraphicsGetImageFromCurrentImageContext()
        
    }
}

