import UIKit

class StartController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var cameraButton: UIButton!
    
    @IBOutlet weak var rollButton: UIButton!
    
    private var imagePicker: StartImagePickerController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        createImagePicker()
        
        EditController.resetEffectsValues()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        cameraButton.titleLabel?.adjustsFontSizeToFitWidth = true
        
        rollButton.titleLabel?.adjustsFontSizeToFitWidth = true
        
        rollButton.titleLabel!.font = rollButton.titleLabel!.font.withSize(rollButton.titleLabel!.font.pointSize * 0.9)
    }

    private func createImagePicker(){
        imagePicker = StartImagePickerController()
        
        imagePicker.delegate = self
    }

    @IBAction func onTakeWithCam(_ sender: UIButton) {
        imagePicker.sourceType = .camera
        
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func onChooseFromCameraRoll(_ sender: UIButton) {
        imagePicker.sourceType = .photoLibrary
        
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        AppDelegate.imageToEdit = image
        
        dismiss(animated: true, completion:
        {
            self.performSegue(withIdentifier: "edit", sender: nil)
            
        })
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    override var prefersStatusBarHidden: Bool{
        return true
    }
}

