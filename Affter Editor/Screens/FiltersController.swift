//
//  BlurController.swift
//  Affter Editor
//
//  Created by Macintosh on 03.10.17.
//  Copyright © 2017 Red Owl. All rights reserved.
//

import UIKit

class FiltersController: UIViewController {

    @IBOutlet weak var starterTexturesButton: BlurButton!
    
    @IBOutlet weak var lightLeaksButton: BlurButton!
    
    @IBOutlet weak var bokehButton: BlurButton!
    
    @IBOutlet weak var colorFog: BlurButton!
    
    @IBOutlet weak var starterTexturesButton2: BlurButton!
    
    @IBOutlet weak var lightLeaksButton2: BlurButton!
    
    @IBOutlet weak var image: UIImageView!
    
    @IBOutlet weak var slider: UISlider!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configurateButtons()
        
        image.image = AppDelegate.editedImage
        
        self.slider.setThumbImage(UIImage.imageWithImage(image: UIImage(named: "6")!, scaledToSize: CGSize(width: 2*slider.bounds.height / 3, height: 2*slider.bounds.height / 3)), for: .normal)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func configurateButtons(){
        starterTexturesButton.imageView?.contentMode = .scaleAspectFit
        
        lightLeaksButton.imageView?.contentMode = .scaleAspectFit
        
        bokehButton.imageView?.contentMode = .scaleAspectFit
        
        colorFog.imageView?.contentMode = .scaleAspectFit
        
        starterTexturesButton2.imageView?.contentMode = .scaleAspectFit
        
        lightLeaksButton2.imageView?.contentMode = .scaleAspectFit
    }
    
    override var prefersStatusBarHidden: Bool{
        return true
    }
}
