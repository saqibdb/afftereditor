//
//  BottomButton.swift
//  Affter Editor
//
//  Created by Macintosh on 03.10.17.
//  Copyright © 2017 Red Owl. All rights reserved.
//

import UIKit

class BottomButton: UIButton {
    private var bottomView: UIView = UIView()
    
    func setSelected(){
        let bottomViewWidth = self.frame.width * 0.4
        
        let bottomViewHeight = self.frame.height * 0.1
        
        bottomView = UIView(frame: CGRect(x: self.bounds.width/2 - bottomViewWidth/2, y: frame.height - bottomViewHeight * 2, width: bottomViewWidth, height: bottomViewHeight))
        
        bottomView = UIView(frame: self.titleLabel!.frame)
        
        bottomView.backgroundColor = .green
        
        self.superview!.addSubview(bottomView)
    }
    
    func deselect(){
        if bottomView.superview != nil{
            bottomView.removeFromSuperview()
        }
    }
}
