//
//  SaveScreenController.swift
//  Affter Editor
//
//  Created by Macintosh on 04.10.17.
//  Copyright © 2017 Red Owl. All rights reserved.
//

import UIKit
import Photos
import SimpleImageViewer
import FBSDKShareKit

class SaveScreenController: UIViewController,UIDocumentInteractionControllerDelegate {
    
    //static let sharedInstance = CustomPhotoAlbum()
    
    var assetCollection: PHAssetCollection!
    
    
    @IBOutlet weak var image: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        image.image = AppDelegate.editedImage
        
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override var prefersStatusBarHidden: Bool{
        return true
    }
    @IBAction func backAction(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func instagramAction(_ sender: UIButton) {
        shareToInstagram()
       
    }
    
    @IBAction func facebookAction(_ sender: UIButton) {
        let photo = FBSDKSharePhoto.init()
        photo.image = image.image
        photo.isUserGenerated = true
        let content : FBSDKSharePhotoContent = FBSDKSharePhotoContent()
        content.photos = [photo]
        FBSDKShareDialog.show(from: self, with: content, delegate: nil)
    }
    
    @IBAction func rateAction(_ sender: UIButton) {
        
        
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            let ac = UIAlertController(title: "Save error", message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        } else {
            let ac = UIAlertController(title: "Saved!", message: "Your Editted image has been saved to your photos.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        }
    }
    
    
    @IBAction func saveAction(_ sender: UIButton) {
        
        
        
        let imageRepresentation = UIImagePNGRepresentation(image.image!)
        let imageData = UIImage(data: imageRepresentation!)
        UIImageWriteToSavedPhotosAlbum(imageData!,  self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    
    
    @IBAction func imageBtnAction(_ sender: UIButton) {
        let configuration = ImageViewerConfiguration { config in
            config.imageView = image
        }
        
        let imageViewerController = ImageViewerController(configuration: configuration)
        
        present(imageViewerController, animated: true)
    }
    
    func shareToInstagram() {
        
        
        //let toSend = croppIngimage((LastImg)!,toRect: CGRect(x: 0, y: offset, width: LastImg!.size.width, height: LastImg!.size.width))
        let imageData = UIImagePNGRepresentation(image.image!) as NSData?
        //let imageData = UIImageJPEGRepresentation(LastImg!, 100)
        let instagramURL = NSURL(string: "instagram://")
        let vkURL = NSURL(string: "vk://")
        let twitterURL = NSURL(string: "twitter://")
        let facebookURL = NSURL(string: "fbapi://")
        if (UIApplication.shared.canOpenURL(instagramURL! as URL)||UIApplication.shared.canOpenURL(vkURL! as URL)||UIApplication.shared.canOpenURL(twitterURL! as URL)||UIApplication.shared.canOpenURL(facebookURL! as URL)) {
            let captionString = "#AfterEditor"
            let writePath = (NSTemporaryDirectory() as NSString).appendingPathComponent("instagram.png")
            if imageData?.write(toFile: writePath, atomically: true) == false {
                
                return
                
            } else {
                
                
                let fileURL = NSURL(fileURLWithPath: writePath)
                
                let documentController = UIDocumentInteractionController(url: fileURL as URL)
                
                documentController.delegate = self
                
                documentController.uti = "com.instagram.photo"
                
                documentController.annotation = NSDictionary(object: captionString, forKey: "TwitterCaption" as NSCopying)
                documentController.presentOpenInMenu(from: self.view.frame, in: self.view, animated: true)
            }
        }
        else{
            print("You Don't Have Twitter, Facebook,VK or Instagram")
        }
        
    }
}
