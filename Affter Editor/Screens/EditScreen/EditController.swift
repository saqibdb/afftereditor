//
//  EditController.swift
//  Affter Editor
//
//  Created by Macintosh on 03.10.17.
//  Copyright © 2017 Red Owl. All rights reserved.
//

import UIKit
import CoreImage
import EVGPUImage2

enum Effect{
    case brightness
    
    case contrast
    
    case saturation
    
    case sharpen
    
    case warmth
    
    case exposure
}

class EditController: UIViewController {
    
    @IBOutlet weak var image: UIImageView!
    
    @IBOutlet weak var editButton: BottomButton!
    
    @IBOutlet weak var brightnessButton: UIButton!
    
    @IBOutlet weak var ContrastButton: UIButton!
    
    @IBOutlet weak var saturationButton: UIButton!
    
    @IBOutlet weak var sharpenButton: UIButton!
    
    @IBOutlet weak var warmthButton: UIButton!
    
    @IBOutlet weak var exposureButton: UIButton!
    
    @IBOutlet weak var slider: UISlider!
    
    @IBOutlet weak var effectLabelImage: UIImageView!
    
    private var currentEffect: Effect = .brightness
    
    @IBOutlet weak var brightnessSelectImage: UIImageView!
    
    @IBOutlet weak var contrastSelectImage: UIImageView!
    
    @IBOutlet weak var saturationSelectImage: UIImageView!
    
    @IBOutlet weak var sharpenSelectImage: UIImageView!
    
    @IBOutlet weak var warmthSelectImage: UIImageView!
    
    @IBOutlet weak var exposureSelectImage: UIImageView!
    
    static var effectsSliderValues: [Effect: CGFloat] =
    [
        .brightness: 0,
        .contrast: 2,
        .saturation: 1,
        .sharpen: 0,
        .warmth: 5000,
        .exposure: 0
    ]
    
    private var queues: [DispatchQueue] = []
    private var items: [DispatchWorkItem] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configurateButtons()
        
        self.image.image = AppDelegate.editedImage
        
        slider.isContinuous = false
        
        self.changeImage()
        
        self.slider.setThumbImage(UIImage.imageWithImage(image: UIImage(named: "6")!, scaledToSize: CGSize(width: 2*slider.bounds.height / 3, height: 2*slider.bounds.height / 3)), for: .normal)
        
        self.deselectAll()
        
        self.brightnessSelectImage.alpha = 1
    }
    
    override func viewWillAppear(_ animated: Bool) {
        editButton.setSelected()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func configurateButtons(){
        self.brightnessButton.imageView?.contentMode = .scaleAspectFit
        
        self.ContrastButton.imageView?.contentMode = .scaleAspectFit
        
        self.saturationButton.imageView?.contentMode = .scaleAspectFit
        
        self.sharpenButton.imageView?.contentMode = .scaleAspectFit
        
        self.warmthButton.imageView?.contentMode = .scaleAspectFit
        
        self.exposureButton.imageView?.contentMode = .scaleAspectFit
    }
    
    override var prefersStatusBarHidden: Bool{
        return true
    }
    
    private func deselectAll(){
        brightnessSelectImage.alpha = 0
        
        contrastSelectImage.alpha = 0
        
        saturationSelectImage.alpha = 0
        
        sharpenSelectImage.alpha = 0
        
        warmthSelectImage.alpha = 0
        
        exposureSelectImage.alpha = 0
    }
    
    @IBAction func onBrightness(_ sender: UIButton) {
        self.effectLabelImage.image = #imageLiteral(resourceName: "brightness")
        
        self.currentEffect = .brightness
        
        self.slider.minimumValue = -0.5
        
        self.slider.maximumValue = 0.5
    self.slider.setValue(Float(EditController.effectsSliderValues[self.currentEffect]!), animated: true)
        
        deselectAll()
        
        brightnessSelectImage.alpha = 1
    }
    
    @IBAction func onContrast(_ sender: UIButton) {
        self.effectLabelImage.image = #imageLiteral(resourceName: "CONTRAST")
        
        self.currentEffect = .contrast
     
        self.slider.minimumValue = 1
        
        self.slider.maximumValue = 3
        
        
        self.slider.setValue(Float(EditController.effectsSliderValues[self.currentEffect]!), animated: true)
        
        deselectAll()
        
        contrastSelectImage.alpha = 1
    }
    
    @IBAction func onSaturation(_ sender: UIButton) {
        self.effectLabelImage.image = #imageLiteral(resourceName: "saturation")
        
        self.currentEffect = .saturation
        
        self.slider.minimumValue = 0
        
        self.slider.maximumValue = 4
        
        self.slider.setValue(Float(EditController.effectsSliderValues[self.currentEffect]!), animated: true)
        
        deselectAll()
        
        saturationSelectImage.alpha = 1
    }
    
    @IBAction func onSharpen(_ sender: Any) {
        self.effectLabelImage.image = #imageLiteral(resourceName: "sharpen")
        
        self.currentEffect = .sharpen
        
        self.slider.setValue(Float(EditController.effectsSliderValues[self.currentEffect]!), animated: true)
        
        deselectAll()
        
        sharpenSelectImage.alpha = 1
    }
    
    @IBAction func onWarmth(_ sender: UIButton) {
        self.effectLabelImage.image = #imageLiteral(resourceName: "warmth")
        
        self.currentEffect = .warmth
        
        slider.minimumValue = 4000
        
        slider.maximumValue = 7000
        
        self.slider.setValue(Float(EditController.effectsSliderValues[self.currentEffect]!), animated: true)
        
        deselectAll()
        
        warmthSelectImage.alpha = 1
    }
    
    @IBAction func onExposure(_ sender: UIButton) {
        self.effectLabelImage.image = #imageLiteral(resourceName: "exposure")
        
        self.currentEffect = .exposure
        
        self.slider.minimumValue = -5
        
        self.slider.maximumValue = 5
        
        
        self.slider.setValue(Float(EditController.effectsSliderValues[self.currentEffect]!), animated: true)
        
        deselectAll()
        
        exposureSelectImage.alpha = 1
    }
    
    @IBAction func sliderValueChanged(_ sender: UISlider) {
        EditController.effectsSliderValues[self.currentEffect] = CGFloat(slider.value)
        
        self.changeImage()
    }
    
    func changeImage(){
        let brightness = BrightnessAdjustment()
        let contrast = ContrastAdjustment()
        let saturation = SaturationAdjustment()
        let exposure = ExposureAdjustment()
        let sharpen = Sharpen()
        let warmth = WhiteBalance()
        
        
        brightness.brightness = Float(EditController.effectsSliderValues[.brightness]!)
        contrast.contrast = Float(EditController.effectsSliderValues[.contrast]!)
        saturation.saturation = Float(EditController.effectsSliderValues[.saturation]!)
        exposure.exposure = Float(EditController.effectsSliderValues[.exposure]!)
        sharpen.sharpness = Float(EditController.effectsSliderValues[.sharpen]!)
        warmth.temperature = Float(EditController.effectsSliderValues[.warmth]!)
        
        let brightnessGroup = OperationGroup()
        
        brightnessGroup.configureGroup{
            input, output in
            
            input --> brightness --> output
        }
        
        let contrastGroup = OperationGroup()
        
        contrastGroup.configureGroup{
            input, output in
            
            input --> contrast --> output
        }
        
        let saturationGroup = OperationGroup()
        
        saturationGroup.configureGroup{
            input, output in
            
            input --> saturation --> output
        }
        
        
        let exposureGroup = OperationGroup()
        
        exposureGroup.configureGroup{
            input, output in
            
            input --> exposure --> output
        }
        
        let sharpnessGroup = OperationGroup()
        
        sharpnessGroup.configureGroup{
            input, output in
            
            input --> sharpen --> output
        }
        
        let warmthGroup = OperationGroup()
        
        warmthGroup.configureGroup{
            input, output in
            
            input --> warmth --> output
        }

        
        DispatchQueue.global(qos: .userInteractive).async {
            var image: UIImage? = nil
            image = AppDelegate.getImage().filterWithOperation(brightnessGroup)
            image = image!.filterWithOperation(contrastGroup)
            image = image!.filterWithOperation(saturationGroup)
            image = image!.filterWithOperation(sharpnessGroup)
            image = image!.filterWithOperation(warmthGroup)
            image = image!.filterWithOperation(exposureGroup)
            
            
            DispatchQueue.main.async(execute: {
                self.image.image = image
                AppDelegate.editedImage = self.image.image
            })
        }
        
        
    }
    
    static func resetEffectsValues(){
        effectsSliderValues = 
        [
        .brightness: 0,
        .contrast: 1,
        .saturation: 1,
        .sharpen: 0,
        .warmth: 5000,
        .exposure: 0
        ]
    }
}
