//
//  BlurController.swift
//  Affter Editor
//
//  Created by Macintosh on 05.10.17.
//  Copyright © 2017 Red Owl. All rights reserved.
//

import UIKit
import EVGPUImage2

enum SliderState{
    case opacity
    
    case strength
}

class BlurController: UIViewController, BlurActivationDelegate {
    func applyBlur() {
        let blur = GaussianBlur()
        if self.slider.value == 0.8 {
            blur.blurRadiusInPixels = 4.0
        }
        else{
            blur.blurRadiusInPixels = self.slider.value
        }
        
        
        print("value changed\(self.slider.value)")

        image.image = AppDelegate.getImage().filterWithOperation(blur)
        
        for curveLine in BlurImage.lines {
            for line in curveLine.getLines() {
                
                UIGraphicsBeginImageContext(image.image!.size)
                
                image.erase(from: line.start, to: line.end, context: UIGraphicsGetCurrentContext()!, lineWidth: line.lineWidth)
                
                UIGraphicsEndImageContext()

            }
        }
    }
    
    
    @IBOutlet weak var image: BlurImageView!
    
    @IBOutlet weak var backgroundImage: UIImageView!
    
    @IBOutlet weak var brushButton: UIButton!
    
    @IBOutlet weak var eraseButton: UIButton!
    
    @IBOutlet weak var strengthButton: UIButton!
    
    @IBOutlet weak var undoButton: UIButton!
    
    @IBOutlet weak var slider: UISlider!
    
    @IBOutlet weak var drawView: DrawView!
    
    @IBOutlet weak var brushSelectImage: UIImageView!
    
    @IBOutlet weak var eraseSelectImage: UIImageView!
    
    @IBOutlet weak var strengthSelectImage: UIImageView!
    
    @IBOutlet weak var actionImage: UIImageView!
    
    private var sliderState: SliderState = .opacity
    
    private let sliderMinBound: [SliderState: CGFloat] =
        [
            .opacity: 0.1,
            .strength: 1
    ]
    
    private let sliderMaxBound: [SliderState: CGFloat] =
        [
            .opacity: 2,
            .strength: 8
    ]
    
    private var sliderValue: [SliderState: CGFloat] =
        [
            .opacity: 1,
            .strength: 4
    ]
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        slider.isContinuous = false
        
        configurateButtons()
        
        self.image.image = AppDelegate.editedImage
        
        self.backgroundImage.image = self.image.image
        
        self.slider.setThumbImage(UIImage.imageWithImage(image: UIImage(named: "6")!, scaledToSize: CGSize(width: 2*slider.bounds.height / 3, height: 2*slider.bounds.height / 3)), for: .normal)
        
        self.slider.isContinuous = false
        
        drawView.backgroundColor = .clear
        
        drawView.bounds.size = (image.image?.size)!
        
        deselectAll()
        
        brushSelectImage.alpha = 1
        
        self.drawView.blurActivationDelegate = self
        
        self.drawView.useBrush = true
        
        self.slider.value = 5.0
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if image.image!.size.height > image.image!.size.width{
            self.image.image = AppDelegate.resizeImage(image: AppDelegate.editedImage, newHeight: self.drawView.bounds.height)
        }
        
        drawView.drawCircle.removeFromSuperview()
        drawView.drawCircle = UIImageView(frame: CGRect(x: drawView.bounds.width/2, y: drawView.bounds.height/2 , width: drawView.lineWidth, height: drawView.lineWidth))
        if drawView.useBrush {
            drawView.drawCircle.image = #imageLiteral(resourceName: "brush-1")
        } else {
            drawView.drawCircle.image = #imageLiteral(resourceName: "eraser-1")
        }
        
        drawView.addSubview(drawView.drawCircle)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        AppDelegate.editedImage = self.image.image
    }
    
    private func configurateButtons(){
        brushButton.imageView?.contentMode = .scaleAspectFit
        
        eraseButton.imageView?.contentMode = .scaleAspectFit
        
        strengthButton.imageView?.contentMode = .scaleAspectFit
        
        undoButton.imageView?.contentMode = .scaleAspectFit
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden: Bool{
        return true
    }
    
    private func deselectAll(){
        self.brushSelectImage.alpha = 0
        
        self.eraseSelectImage.alpha = 0
        
        self.strengthSelectImage.alpha = 0
    }
    
    @IBAction func onBrush(_ sender: UIButton) {
        drawView.changeColor(color: .green)

        self.drawView.useBrush = true
        
        drawView.isDrawing = true
        
        self.actionImage.image = #imageLiteral(resourceName: "opacity")
        
        deselectAll()
        
        brushSelectImage.alpha = 1
        
        if sliderState == .strength{
            self.sliderValue[.strength] = CGFloat(slider.value)
        }else{
            self.sliderValue[.opacity] = CGFloat(slider.value)
        }
        
        self.setSliderBounds(min: sliderMinBound[.opacity]!, max: sliderMaxBound[.opacity]!)
        
        sliderState = .opacity
        
        self.slider.setValue(Float(sliderValue[.opacity]!), animated: true)
    }
    
    private func setSliderBounds(min: CGFloat, max: CGFloat){
        slider.minimumValue = Float(min)
        slider.maximumValue = Float(max)
    }
    
    @IBAction func onErase(_ sender: UIButton) {
        drawView.changeColor(color: .green)

        self.drawView.useBrush = false
        
        drawView.isDrawing = true
        
        self.actionImage.image = #imageLiteral(resourceName: "opacity")
        
        deselectAll()
        
        eraseSelectImage.alpha = 1
        
        if sliderState == .strength{
            self.sliderValue[.strength] = CGFloat(slider.value)
        }else{
            self.sliderValue[.opacity] = CGFloat(slider.value)
        }
        
        self.setSliderBounds(min: sliderMinBound[.opacity]!, max: sliderMaxBound[.opacity]!)
        
        sliderState = .opacity
        
        self.slider.setValue(Float(sliderValue[.opacity]!), animated: true)
    }
    
    @IBAction func onStrength(_ sender: UIButton) {
        
        
        drawView.changeColor(color: .clear)
        
        self.drawView.drawColor = .clear
        
        drawView.isDrawing = false
        
        self.actionImage.image = #imageLiteral(resourceName: "strength")
        
        deselectAll()
        
        if sliderState == .opacity{
            self.sliderValue[.opacity] = CGFloat(slider.value)
        }
        
        self.setSliderBounds(min: sliderMinBound[.strength]!, max: sliderMaxBound[.strength]!)
        
        sliderState = .strength
        
        self.slider.setValue(Float(sliderValue[.strength]!), animated: true)
        
        strengthSelectImage.alpha = 1
    }
    
    @IBAction func onUndo(_ sender: UIButton) {
        if !drawView.haveLines {
            self.image.image = AppDelegate.editedImage
            
            return
        }
        
        self.drawView.undoLast()
    }
    
    @IBAction func onEdit(_ sender: UIButton) {
        AppDelegate.editedImage = self.mergeImages()
        AppDelegate.imageToEdit = AppDelegate.editedImage
    }
    
    @IBAction func onFilters(_ sender: UIButton) {
        AppDelegate.editedImage = self.mergeImages()
        AppDelegate.imageToEdit = AppDelegate.editedImage
    }
    
    @IBAction func onSave(_ sender: UIButton) {
        AppDelegate.editedImage = self.mergeImages()
        AppDelegate.imageToEdit = AppDelegate.editedImage
    }
    
    @IBAction func sliderValueChanged(_ sender: UISlider) {
        
        
        
        if sliderState == .opacity{
            self.drawView.lineWidth = CGFloat(40 * sender.value + 1)
        }else{
            let blur = GaussianBlur()
            blur.blurRadiusInPixels = self.slider.value
            image.image = AppDelegate.getImage().filterWithOperation(blur)
            
            
            print("value changed\(self.slider.value)")

            for curveLine in BlurImage.lines {
                for line in curveLine.getLines() {
                    
                    
                    UIGraphicsBeginImageContext(image.image!.size)
                    
                    image.erase(from: line.start, to: line.end, context: UIGraphicsGetCurrentContext()!, lineWidth: line.lineWidth)
                    
                    UIGraphicsEndImageContext()
                    
                }
            }
            //self.drawView.undoAll()
        }
    }
    
    func mergeImages() -> UIImage {
        let imageSize = image.image!.size
        
        UIGraphicsBeginImageContext(imageSize)
        self.backgroundImage.image?.draw(in: CGRect(x: 0, y: 0, width: imageSize.width, height: imageSize.height))
        self.image.image?.draw(in: CGRect(x: 0, y: 0, width: imageSize.width, height: imageSize.height))
        return UIGraphicsGetImageFromCurrentImageContext()!
    }
    
    
    @IBAction func homeAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
