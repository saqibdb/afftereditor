//
//  CurveLine.swift
//  Affter Editor
//
//  Created by Roman on 06.11.2017.
//  Copyright © 2017 Red Owl. All rights reserved.
//

import Foundation

final class CurveLine {
    private var lines: [Line] = []
    
    func addLine(line: Line) {
        lines.append(line)
    }
    
    func getLines() -> [Line] {
        return lines
    }
    
    func remove(at: Int) {
        self.lines.remove(at: at)
    }
}
